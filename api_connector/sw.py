import logging

import requests

from api_connector.starship import Starship


class SwApiConnector:
    first_page = 'https://swapi.co/api/starships/?page=1'
    logger = logging.getLogger('SwApiConnector')

    def parse_json(self, url):
        """
        Parse specified url and return json or raise exception on error
        :param url: url to parse
        :return: response (json) or exception
        """
        resp = requests.get(url)
        if resp.status_code == 200:
            return resp.json()
        else:
            raise Exception

    def prepare_response(self, starships, unknown_starships):
        """
        Prepare response
        :param starships: list of starships
        :param unknown_starships: list of starships with hyperdrive unknown
        :return: dict
        """
        return {'starships': [x.to_json() for x in starships],
                'starships_unknown_hyperdrive': [x.to_json() for x in unknown_starships]}

    def parse_all(self):
        """
        Main method.
        Parses all available pages and return pretty response
        :return: dict with list of sorted starships by hyperdrive rating and list of starships with unknown hyperdrive
        """
        starships = []
        unknown_starships = []
        url = self.first_page
        while True:
            if not url:
                break
            try:
                page = self.parse_json(url)
                for starship in page.get('results'):
                    hyperdrive_rating = starship.get('hyperdrive_rating')
                    name = starship.get('name')
                    if hyperdrive_rating == 'unknown':
                        unknown_starships.append(Starship(name, None))
                    else:
                        starships.append(Starship(name, float(hyperdrive_rating)))
                url = page.get('next', None)
            except Exception as e:
                self.logger.exception(e)
                break
        starships = sorted(starships)
        return self.prepare_response(starships, unknown_starships)
