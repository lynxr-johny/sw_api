
class Starship:
    def __init__(self, name, hyperdrive_rating=None) -> None:
        self.name = name
        self.hyperdrive_rating = hyperdrive_rating

    def __gt__(self, other):
        return self.hyperdrive_rating > other.hyperdrive_rating

    def __lt__(self, other):
        return self.hyperdrive_rating < other.hyperdrive_rating

    def __le__(self, other):
        return self.hyperdrive_rating <= other.hyperdrive_rating

    def __ge__(self, other):
        return self.hyperdrive_rating >= other.hyperdrive_rating

    def __eq__(self, other):
        return self.hyperdrive_rating == other.hyperdrive_rating

    def to_json(self):
        resp = {'name': self.name}
        if self.hyperdrive_rating:
            resp['hyperdrive'] = self.hyperdrive_rating
        return resp
