import unittest

from api_connector.starship import Starship
from api_connector.sw import SwApiConnector


class SwApiTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.sw = SwApiConnector()

    def test_sorting(self):
        starships = [
            Starship('2', 0.6),
            Starship('1', 0.1)
        ]
        sorted_starships = sorted(starships)
        self.assertEqual(sorted_starships[0].name, '1')
        self.assertEqual(sorted_starships[1].name, '2')

    def test_prepare_response(self):
        starships = [Starship('1', 1)]
        unknown_starships = [Starship('2')]
        expected_dict = {
            'starships': [{'name': '1', 'hyperdrive': 1}],
            'starships_unknown_hyperdrive': [{'name': '2'}]
        }
        resp = self.sw.prepare_response(starships, unknown_starships)
        self.assertDictEqual(resp, expected_dict)
