import logging

from flask import Flask, jsonify

from api_connector.sw import SwApiConnector

app = Flask(__name__)
parser = SwApiConnector()
logging.basicConfig(level=logging.DEBUG)


@app.route('/')
def hello_world():
    return jsonify(parser.parse_all())


if __name__ == '__main__':
    app.run()
