# Install guide
- pip install -r requirements.txt

## Run in development mode
- export FLASK_APP=app.py
- export FLASK_ENV=development
- flask run
- go to http://127.0.0.1:5000

## Run in production mode
- gunicorn -w 4 app:app
- go to http://127.0.0.1:8000
